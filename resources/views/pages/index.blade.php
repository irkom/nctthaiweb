@extends('layouts.app')
@section('content')
<!--Carousel Wrapper-->
<div id="mycarousel" class="carousel slide carousel-fade white-text" data-ride="carousel" data-interval="false">
   <!--Indicators-->
   <ol class="carousel-indicators">
      <li data-target="#mycarousel" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-3" data-slide-to="1"></li>
   </ol>
   <!--/.Indicators-->
   <!--Slides-->
   <div class="carousel-inner" role="listbox">
      <!-- First slide -->
      <div class="carousel-item active view hm-black-light" style="background-image: url({{asset('img/bg_wai.png')}}); background-repeat: no-repeat; background-size: cover;">
         <!-- Caption -->
         <div class="full-bg-img flex-center white-text">
            <ul class="animated fadeInUp col-md-12">            
                <li>
                    
                </li>
            </ul>
         </div>
         <!-- /.Caption -->
      </div>
      <!-- /.First slide --> 
      <!-- Second slide -->
            <div class="carousel-item view hm-black-light" style="background-image: url({{asset('img/bg2.png')}}); background-repeat: no-repeat; background-size: cover;">

                <!-- Caption -->
                <div class="full-bg-img flex-center white-text">
                    <ul class="animated fadeInUp col-md-12">
                        <li>
                            <h1 class="h1-responsive">We are looking for a new member</h1>
                        </li>                
                        <li>
                            <a href="/career" class="btn btn-primary btn-lg" rel="nofollow">Join Now</a>
                        </li>
                    </ul>
                </div>
                <!-- /.Caption -->

            </div>
            <!-- /.Second slide --> 
   </div>
   <!--/.Slides-->

   <!--Controls-->
        <a class="carousel-control-prev" href="#mycarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#mycarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
</div>  
<!--/.Carousel Wrapper-->
<!-- Main container-->
<div class="container">
    <div id="aboutus">
        <div class="divider-new" >
            <h2 class="h2-responsive wow fadeIn" data-wow-delay="0.2s">About us</h2>
        </div>
        <!--Section: About-->
        <section id="about" class="text-center wow fadeIn" data-wow-delay="0.2s">
            <div class="row">
                <!--First columnn-->
                <div class="col-lg-4">
                    <!--Card-->
                    <div class="card wow fadeIn thumbnail z-depth-5">
                    <!--Card image-->
                    <div class="view overlay mx-auto d-block hm-white-slight ">
                        <img src="{{asset('img/develop.png')}}" class="img-fluid img-responsive" alt="">
                        <a>
                            <div class="mask"></div>
                        </a>
                    </div>
                    <!--/.Card image-->
                    <!--Card content-->
                    <div class="card-block">
                        <!--Title-->
                        <h4 class="card-title text-center">Software Development</h4>
                        <hr>
                        <!--Text-->
                        <p class="card-text text-justify">&emsp;&emsp;In order to meet our customer’s requirements, we are able to provide system development and services suitable for every type of business.</p>
                    </div>
                    <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <!--First columnn-->
                <!--Second columnn-->
                <div class="col-lg-4">
                    <!--Card-->
                    <div class="card wow fadeIn thumbnail z-depth-5" data-wow-delay="0.2s">
                    <!--Card image-->
                    <div class="view overlay mx-auto d-block hm-white-slight">
                        <img src="{{asset('img/outsource.png')}}" class="img-fluid img-responsive" alt="">
                        <a>
                            <div class="mask"></div>
                        </a>
                    </div>
                    <!--/.Card image-->
                    <!--Card content-->
                    <div class="card-block">
                        <!--Title-->
                        <h4 class="card-title text-center">Outsourcing Service</h4>
                        <hr>
                        <!--Text-->
                        <p class="card-text text-justify">&emsp;&emsp;We provide outsourcing service by sending our engineers to work at the customer’s IT department to directly support the system development.</p>
                    </div>
                    <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <!--Second columnn-->
                <!--Third columnn-->
                <div class="col-lg-4">
                    <!--Card-->
                    <div class="card wow fadeIn thumbnail z-depth-5" data-wow-delay="0.4s">
                    <!--Card image-->
                    <div class="view overlay mx-auto d-block hm-white-slight">
                        <img src="{{asset('img/customer_service.png')}}" class="img-fluid img-responsive" alt="">
                        <a>
                            <div class="mask"></div>
                        </a>
                    </div>
                    <!--/.Card image-->
                    <!--Card content-->
                    <div class="card-block text-center">
                        <!--Title-->
                        <h4 class="card-title text-center">Desktop & End user support</h4>
                        <hr>
                        <!--Text-->
                        <p class="card-text text-justify">&emsp;&emsp;Our staff has experience providing professional support services to customers of a variety of cultures, languages, and business fields.</p>
                        <p class="card-text text-justify">&emsp;&emsp;Our experienced staff are comfortable serving customer's in English, Japanese, and Thai.</p>
                    </div>
                    <!--/.Card content-->
                    </div>
                    <!--/.Card-->
                </div>
                <!--Third columnn-->
        </section>
        <!--Section: About-->
   </div>
   <div id="ourcustomers">
      <div class="divider-new"  >
         <h2 class="h2-responsive wow fadeIn">Our Customers</h2>
      </div>
      <!--Section: Our Customers-->
      <section class="text-center z-depth-5" id="customers">
         <!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
         <div class="row img-responsive">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-2 ">
               <img src="{{asset('img/partner/fujitsu.png')}}" alt="..." class="rounded-top img-responsive">
            </div>
            <div class="col-md-2">
               <img src="{{asset('img/partner/subsiri.png')}}" alt="..." class="rounded-top img-responsive">
            </div>
            <div class="col-md-2">
               <img src="{{asset('img/partner/hitachi.png')}}" alt="..." class="rounded-top img-responsive">
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-1">
            </div>
         </div>
         <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-2" id="bjc">
               <img src="{{asset('img/partner/bjc.png')}}" alt="..." class="rounded-top img-responsive">
            </div>
            <div class="col-md-2">
               <img src="{{asset('img/partner/asiabooks.png')}}" alt="..." class="rounded-top img-responsive">
            </div>
            <div class="col-md-2">
               <img src="{{asset('img/partner/broger.png')}}" alt="..." class="rounded-top img-responsive">
            </div>
            <div class="col-md-2">
            </div>
            <div class="col-md-1">
            </div>
         </div>
      </section>
      <!--/Section: Best features-->
   </div>

    <!--Section: Call to action-->
    <section>

        <br>
        <hr>
        <h3 class="h2-responsive text-center wow fadeIn">Are you ready to join with us?</h3>
        <br>
        <div class="text-center wow fadeIn">
            <a class="btn btn-primary btn-lg" href="/career" data-toggle="modal" data-target="#contactForm">Apply now!</a>
        </div>

    </section>
    <!--/Section: Call to action-->

   <div id="contactus">
      <div class="divider-new">
         <h2 class="h2-responsive wow fadeIn">Contact us</h2>
      </div>
      <!--Section: Contact-->
      <section id="contact">
         <div class="row">
            <!--First column-->
            <div class="col-lg-8 ">
               <div id="map-container" class="z-depth-2 wow fadeIn" style="height: 300px"></div>
            </div>
            <!--/First column-->
            <!--Second column-->
            <div class="col-lg-4">
               <ul class="text-center">
                  <li class="wow fadeIn" data-wow-delay="0.2s">
                     <i class="fa fa-map-marker teal-text"></i>
                     <p>Charn Issara Tower 1, Suite No.942/42, Plaza Floor. Rama4 Road, Suriyawong, Bangrak, Bangkok, Thailand 10500</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.3s">
                     <i class="fa fa-phone teal-text"></i>
                     <p>02-115-9787</p>
                  </li>
                  <li class="wow fadeIn" data-wow-delay="0.4s">
                     <i class="fa fa-envelope teal-text"></i>
                     <p>ctofficial@nctthai.com</p>
                  </li>
               </ul>
            </div>
            <!--/Second column-->
         </div>
      </section>
      <!--Section: Contact-->
   </div>

   
</div>
<!--/ Main container-->
<hr>
<!--Footer-->
<footer class="page-footer center-on-small-only">
   <!--Copyright-->
   <div class="footer-copyright">
      <div class="container-fluid">
         © 2017 Copyright: <a href="http://nctthai.com"> New Computer Technology Consulting Co., LTD</a>
      </div>
   </div>
   <!--/.Copyright-->
</footer>
<!--/.Footer-->