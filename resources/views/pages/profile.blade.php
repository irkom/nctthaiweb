@extends('layouts.apl')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">        
            <img src="/img/{{$user->avatar}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">   
            <h2>{{$user->name}}'s Profile</h2>
            <form enctype="multipart/form-data" action="/profile" method="POST" class="form-group">
                <label>Update Profile Image</label>

                <div class="input-group input-group-sm">
                    <span class="input-group-addon">Email</span>
                    <input name="email" type="email" class="form-control" placeholder="Your new email" aria-describedby="sizing-addon1" value="{{$user->email}}">
                </div>

                <div class="input-group input-group-sm">
                    <span class="input-group-addon" >Browse</span>
                    <input type="file" name="avatar" class="form-control"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" >                
                </div>
                <input type="submit" class="pull-right btn btn-sm btn-primary">

                
            </form>
        </div>
    </div>
</div>
@endsection
