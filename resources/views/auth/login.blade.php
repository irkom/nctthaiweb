@extends('layouts.apl')

@section('content')
<div class="container" id="login"> 
    <div class="row">
    <div class="col-lg-4 col-md-8">
    </div>
    <div class="col-lg-4 col-md-8">
        <!--Form without header-->
        <div class="card">
            <div class="card-block">

                <!--Header-->
                <div class="text-center">
                    <h3><i class="fa fa-lock"></i> Login:</h3>
                    <hr class="mt-2 mb-2">
                </div>

                <!--Body-->
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                    <div class="md-form">
                        <i class="fa fa-envelope prefix"></i>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                        <label for="email" data-error="wrong" data-success="right">Your email</label>                   
                    </div>
                    <div class="md-form">
                        <i class="fa fa-lock prefix"></i>
                        <input type="password" id="password" name="password" class="form-control">
                        <label for="password" data-error="wrong" data-success="right">Your password</label>
                    </div>

                    <fieldset class="form-group text-center">
                        <input class="" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember">remember me</label>                        
                        <div class="text-center">
                            <button class="btn btn-default">Login</button>
                        </div>
                    </fieldset>
                    
                    <!--Footer-->
                    <div class="modal-footer">
                        <div class="options">
                            <p>Forgot <a href="{{ route('password.request') }}">Password?</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-4 col-md-8">
        </div>
    </div>
</div>
<!--/Form without header-->
</div>

@endsection
