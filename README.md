####-Installation project on your localhost
- ```$ composer install ```

- ```$ php artisan key:generate ```

- setup your database with modify ```.env```

- run migrate to generate table and data ```$ php artisan migrate:refresh --seed```
